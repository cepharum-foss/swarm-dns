/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const HTTP = require( "http" );
const Client = require( "dns" );
const Server = require( "@cepharum/dnsd" );


console.log( "STATUS_PROVIDER is '%s'", process.env.STATUS_PROVIDER || "" );
console.log( "UPDATE_INTERVAL is '%s'", process.env.UPDATE_INTERVAL || "10" );
console.log( "LOCAL_NS is '%s'", process.env.LOCAL_NS || "" );
console.log( "TRUSTED_NS is '%s'", process.env.TRUSTED_NS || "" );
console.log( "TRUSTED_ACCEPT_NODATA is '%s'", process.env.TRUSTED_ACCEPT_NODATA || "yes" );
console.log( "TRUSTED_REJECT_TIMEOUT is '%s'", process.env.TRUSTED_REJECT_TIMEOUT || "yes" );


const statusProviderURL = String( process.env.STATUS_PROVIDER || "" ).trim();
if ( !statusProviderURL.length ) {
	console.error( "URL of status provider must be provided in env variable STATUS_PROVIDER" );
	process.exit( 1 );
}

const url = statusProviderURL === "test" ? false : new URL( statusProviderURL );
const Source = {
	host: url ? url.hostname : process.env.TEST_SOURCE_HOSTNAME || "",
	port: url ? url.port : process.env.TEST_SOURCE_PORT || 80,
	path: url ? url.pathname : process.env.TEST_SOURCE_PATH || "/test",
};

const ptnTrue = /^(?:y(?:es)?|t(?:rue)?|1|on)$/i;
const TrustedNSAcceptNoData = ptnTrue.test( String( process.env.TRUSTED_ACCEPT_NODATA || "yes" ).trim() );
const TrustedNSRejectTimeout = ptnTrue.test( String( process.env.TRUSTED_REJECT_TIMEOUT || "yes" ).trim() );
const TrustedNSTimeout = 3;
const DefaultATTL = 600;
const DefaultNSTTL = 86400;


let latestRevision = 0;
const CachedNodes = [];

const TrustCache = {};


Promise.all( [
	Promise.all(
		String( process.env.TRUSTED_NS || "" ).trim().split( /[\s,;]+/ )
			.filter( i => i )
			.map( name => new Promise( ( resolve, reject ) => {
				Client.resolve4( name, ( error, addresses ) => {
					if ( error ) {
						reject( error );
					} else {
						console.log( "trusted NS: %s => %j", name, addresses );
						resolve( addresses );
					}
				} );
			} ) ),
	),
	Promise.all(
		String( process.env.LOCAL_NS || "" ).trim().split( /[\s,;]+/ )
			.filter( i => i )
			.map( name => new Promise( ( resolve, reject ) => {
				Client.resolve4( name, ( error, addresses ) => {
					if ( error ) {
						reject( error );
					} else {
						console.log( "local NS: %s => %j", name, addresses );
						resolve( { name, addresses } );
					}
				} );
			} ) ),
	),
] )
	.then( ( [ trustedNsAddresses, LocalNameServers ] ) => {
		if ( LocalNameServers.length < 2 ) {
			throw new Error( "list of local name servers must contain at least two servers" );
		}

		const TrustedNameServers = [].concat( ...trustedNsAddresses );

		console.log( "trusting name servers: %j", TrustedNameServers );

		return { TrustedNameServers, LocalNameServers };
	} )
	.then( ( { TrustedNameServers, LocalNameServers }) => {
		let latestRequestId = 0;

		const dnsService = Server.createServer( ( req, res ) => {
			const requestId = ++latestRequestId;

			// don't respond to queries unless having response data, by default
			res.ignoreEmpty = true;

			console.debug( "%d: handling new query", requestId );

			processQuestions( req.question )
				.then( ( { answers, additionals, authoritatives }) => {
					console.log( "%d: compiling response", requestId );

					for ( const answer of answers ) {
						res.answer.push( answer );
					}

					for ( const additional of additionals ) {
						res.additional.push( additional );
					}

					for ( const authoritative of authoritatives ) {
						res.authority.push( authoritative );
					}

					console.debug( "%d: compiled response", requestId );
				} )
				.catch( error => {
					console.error( "%d: handling query failed: %s", requestId, error.stack );

					res.responseCode = require( "@cepharum/dnsd/constants" ).RCODE.SERVFAIL;
				} )
				.finally( () => {
					console.debug( "%d: ending response", requestId );

					res.end();
				} );


			function processQuestions( questions ) {
				const delivered = { answers: {}, additionals: {}, authoritatives: {} };
				const answers = [];
				const additionals = [];
				const authoritatives = [];

				return new Promise( ( resolve, reject ) => {
					processQuestion( questions, 0, questions.length );

					function processQuestion( list, current, stopAt ) {
						if ( current >= stopAt ) {
							resolve( { answers, additionals, authoritatives } );
							return;
						}

						const question = list[current];

						if ( !question ) {
							reject( new Error( `invalid request including nullish question #${current}` ) );
							return;
						}

						const { name, type } = question;

						if ( !name || typeof name !== "string" ) {
							reject( new Error( `invalid request including nullish name in question #${current}` ) );
							return;
						}

						// handle request
						switch ( type ) {
							case "ANY" :
							case "*" :
							case "A" :
							case "SOA" :
							case "NS" :
								res.ignoreEmpty = false;
								testIfAuthoritative( name )
									.then( authoritative => {
										if ( authoritative ) {
											console.log( "%d: %s %s", requestId, type, name );

											collectResponse( name, type );
										}

										process.nextTick( processQuestion, list, current + 1, stopAt );
									} )
									.catch( reject );
								return;

							case "CAA" :
								// CAA RRs are tested starting at specific domain to be certified traversing through parent domains
								// -> mustn't ignore this request but have to respond with empty result
								res.ignoreEmpty = false;
								break;

							default :
								console.log( "%d: ignoring requests for any type but A records (got: %s)", requestId, type );
								break;
						}

						process.nextTick( processQuestion, list, current + 1, stopAt );
					}
				} );

				function collectResponse( name, rrType ) {
					const localAnswers = [];
					const localAdditionals = [];
					const localAuthoritatives = [];

					switch ( rrType ) {
						case "A" :
							respondA( name, localAnswers );
							respondNS( name.replace( /^[^.]+\./, "" ), localAuthoritatives, localAdditionals );
							break;

						case "SOA" :
							respondSOA( name, localAnswers );
							respondNS( name, localAuthoritatives, localAdditionals );
							break;

						case "NS" :
							respondNS( name, localAnswers, localAdditionals );
							break;

						case "ANY" :
						case "*" :
							for ( const type of [ "A", "SOA", "NS" ] ) {
								collectResponse( name, type );
							}
							return;

						default :
							console.log( "%d: no such response type: %s", requestId, rrType );
							return;
					}

					mergeFrom( delivered.answers, answers, localAnswers );
					mergeFrom( delivered.additionals, additionals, localAdditionals );
					mergeFrom( delivered.authoritatives, authoritatives, localAuthoritatives );
				}

				function respondA( name, aAnswers ) {
					const shuffled = shuffleArray( CachedNodes ).slice( 0, 8 );
					for ( let j = 0; j < shuffled.length; j++ ) {
						aAnswers.push( {
							name,
							class: "IN",
							type: "A",
							ttl: DefaultATTL,
							data: shuffled[j],
						} );
					}

					return `A:${name}`;
				}

				function respondSOA( name, soaAnswers, soaAdditionals ) {
					const master = LocalNameServers[0];
					const now = new Date();
					const serial = now.getUTCFullYear() +
					               String( "0" + ( now.getUTCMonth() + 1 ) ).slice( -2 ) +
					               String( "0" + now.getUTCDate() ).slice( -2 ) +
					               String( "0" + Math.floor( ( now.getUTCHours() * 60 + now.getUTCMinutes() ) / 1440 * 100 ) ).slice( -2 );

					soaAnswers.push( {
						name,
						class: "IN",
						type: "SOA",
						ttl: DefaultATTL,
						data: {
							mname: master.name,
							rname: process.env.SOA_MAIL_ADDRESS || `hostmaster.${name}`,
							serial,
							refresh: DefaultATTL,
							retry: DefaultATTL / 2,
							expire: 604800,
							ttl: 300,
						},
					} );

					if ( Array.isArray( soaAdditionals ) ) {
						for ( const ip of master.addresses ) {
							soaAdditionals.push( {
								name: master.name,
								class: "IN",
								type: "A",
								ttl: DefaultATTL,
								data: ip,
							} );
						}
					}

					return `SOA:${name}`;
				}

				function respondNS( name, nsAnswers, nsAdditionals ) {
					for ( const ns of LocalNameServers ) {
						nsAnswers.push( {
							name,
							class: "IN",
							type: "NS",
							ttl: DefaultNSTTL,
							data: ns.name,
						} );

						if ( Array.isArray( nsAdditionals ) ) {
							for ( const ip of ns.addresses ) {
								nsAdditionals.push( {
									name: ns.name,
									class: "IN",
									type: "A",
									ttl: DefaultATTL,
									data: ip,
								} );
							}
						}
					}

					return `NS:${name}`;
				}

				function mergeFrom( cache, collector, collectibles ) {
					if ( Array.isArray( collector ) && Array.isArray( collectibles ) ) {
						for ( const entry of collectibles ) {
							const key = `${entry.type}:${entry.name}:${typeof entry.data === "string" ? entry.data : "*"}`;

							if ( !cache[key] ) {
								cache[key] = entry;

								collector.push( entry );
							}
						}
					}
				}

				/**
				 * Checks if current service is authoritative for provided name.
				 *
				 * @param {string} name name to check
				 * @returns {Promise<boolean>} promises information whether current service is authoritative for name or not
				 */
				function testIfAuthoritative( name ) {
					const lName = name.toLowerCase();
					let cache = TrustCache[lName];

					if ( cache && Date.now() - cache.time > cache.ttl ) {
						console.log( "%d: dropping cached record on %s", requestId, name );
						cache = TrustCache[lName] = null;
					}

					if ( !cache ) {
						console.log( "%d: checking status of %s with trusted NS", requestId, name );
						cache = TrustCache[lName] = {
							time: Date.now(),
							ttl: Infinity,
							check: testDomainAtTrustedNS( lName )
								.then( ( { authoritative, ttl } ) => {
									console.log( "%d: feeling %sauthoritative for %s for %d seconds", requestId, authoritative ? "" : "not ", name, ttl );

									cache.ttl = ttl * 1000;

									return authoritative;
								} )
								.catch( error => {
									console.error( "%d: testing %s at trusted NS failed: %s", requestId, name, error.stack );

									cache.ttl = 30 * 1000;

									return false;
								} ),
						};
					}

					return cache.check;
				}

				/**
				 * Checks back with some trusted name servers whether current
				 * one is authoritative for selected name or not.
				 *
				 * Trusted nameservers are expected to respond on names they are
				 * authoritative for, only. It shouldn't resolve names on its
				 * own ("it must not be recursive DNS service").
				 *
				 * @param {string} name domain to look up
				 * @returns {Promise<{authoritative: boolean, ttl: number}>} promises cacheable information whether current server is authoritative or not
				 */
				function testDomainAtTrustedNS( name ) {
					if ( TrustedNameServers.length > 0 ) {
						const resolver = new Client.Resolver();
						resolver.setServers( TrustedNameServers );

						console.log( "%d: checking A of %s at %j", requestId, name, TrustedNameServers );

						return Promise.race( [
								new Promise( ( resolve, reject ) => {
									resolver.resolve4( name, { ttl: true }, ( error, records ) => {
										if ( error ) {
											reject( error );
										} else {
											resolve( records );
										}
									} );
								} ),
								new Promise( ( _, onTimeout ) => setTimeout( () => {
									onTimeout( Object.assign( new Error( "request timed out" ), { code: Client.TIMEOUT } ) );
								}, TrustedNSTimeout * 1000 ) )
									.finally( () => resolver.cancel() ),
							] )
								.then( answers => ( {
									authoritative: ( TrustedNSAcceptNoData ? true : answers.length > 0 ),
									ttl: ( Math.max( 0, Math.min( ...answers.map( answer => answer.ttl || 0 ) ) ) || DefaultATTL ),
								} ) )
								.catch( error => {
									switch ( error.code ) {
										case Client.NODATA :
											if ( TrustedNSAcceptNoData ) {
												console.log( "%d: resolving A of %s yielded no answer, but assuming referral", requestId, name,  );
												return { authoritative: true, ttl: DefaultATTL };
											}
											break;

										case Client.TIMEOUT :
											if ( TrustedNSRejectTimeout ) {
												console.log( "%d: resolving A of %s timed out, but assuming request intentionally ignored", requestId, name );
												return { authoritative: false, ttl: DefaultATTL };
											}
											break;
									}

									throw error;
								} );
					}

					return Promise.resolve( { authoritative: true, ttl: Infinity } );
				}
			}
		}, { ttl: 60 } );

		const port = process.env.LISTEN_PORT || 53;
		const ip = process.env.LISTEN_IP || "0.0.0.0";

		dnsService.listen( port, ip, () => {
			console.log( `now listening at ${ip}:${port}` );
		} );


		process.nextTick( fetchNodes, Source );
		setInterval( fetchNodes, ( parseInt( process.env.UPDATE_INTERVAL ) || 10 ) * 1000, Source );
	} )
	.catch( error => {
		console.error( "setting up service failed: %s", error.stack );
		process.exitCode = 1;
	} );



/**
 * @typedef {object} TCPSource
 * @property {string} host hostname of source
 * @property {int} port port of source
 */

/**
 * Fetches list of active nodes' public IP addresses from Docker API.
 *
 * @param {TCPSource} source source exposing Docker API
 * @returns {Promise<void>} promises up-to-date list of available nodes' public IPs in global `CachedNodes`
 */
function fetchNodes( source ) {
	if ( !source.host ) {
		CachedNodes.splice( 0, CachedNodes.length, ...[ "127.0.10.10", "127.0.10.20", "127.0.10.30" ] );

		return Promise.resolve();
	}

	new Promise( ( resolve, reject ) => {
		const options = Object.assign( {}, source, {
			method: "GET",
			path: "/",
		} );

		const request = HTTP.request( options, response => {
			const chunks = [];

			response.once( "error", reject );
			response.on( "data", chunk => chunks.push( chunk ) );
			response.once( "end", () => {
				try {
					resolve( JSON.parse( Buffer.concat( chunks ).toString( "utf8" ) ) );
				} catch ( error ) {
					reject( error );
				}
			} );
		} );

		request.once( "error", reject );
		request.end();
	} )
		.then( data => {
			if ( !data || !data.revision || !Array.isArray( data.nodes ) ) {
				console.error( "ignoring invalid status information" );
				return;
			}

			if ( data.revision <= latestRevision ) {
				return;
			}

			console.log( "got updated nodes %j", data.nodes );

			latestRevision = data.revision;
			CachedNodes.splice( 0, CachedNodes.length, ...data.nodes );
		} )
		.catch( error => {
			console.error( "fetching list of nodes failed: %s", error.stack );
		} );
}

/**
 * Shuffles copy of provided array.
 *
 * @param {array} list set of items to be shuffled
 * @returns {array} copy of items in shuffled order
 */
function shuffleArray( list ) {
	const numItems = list.length;
	const source = list.slice( 0 );
	const shuffled = new Array( numItems );

	for ( let i = 0; i < numItems; i++ ) {
		const index = Math.floor( Math.random() * source.length );
		shuffled[i] = source.splice( index, 1 )[0];
	}

	return shuffled;
}
