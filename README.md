# Swarm DNS

Implements DNS service for use in combination with a Docker Swarm responding with list of active swarm nodes on every request for some A record.

## License

MIT

## Credits

This project has been inspired by [https://github.com/djmaze/swarmdns](https://github.com/djmaze/swarmdns).

However, integrating DNS service with swarm nodes didn't work well, e.g. response times were pretty high and lots of queries got lost. So we decided to separate the DNS service from the swarm using status information provided by [different service](https://github.com/cepharum/swarm-status) there.

## Usage

You should perform the following steps on at least two servers prepared to run docker containers at least.

1. Pick a server to be one of your swarm's DNS servers.

2. Create a script **run.sh** and paste content of [run.sh](run.sh).
   
3. Adjust the listed environment variables to suit your particular environment.

   * `YOUR_IP` **should** provide the public IP address of your docker-enabled host to listen on for incoming requests. You may keep it unset for using current IP of `eth0`.

   * `STATUS_PROVIDER` **must** contain public URL of running [swarm-status service](https://github.com/cepharum/swarm-status) or some compatible API.

   * `LOCAL_NS` **must** contain comma-separated list of all your swarm's DNS servers' fully qualified domain names. 
   
      The service is processing this information in response to requests for NS or SOA records.

   * `UPDATE_INTERVAL` **may** be used to control interval in seconds to check _swarm status service_ for updated list of active swarm nodes. It is 10 seconds by default.

   * `TRUSTED_NS` **may** be set to enable additional security feature. It is meant to list one or more IP addresses of trusted name servers. 
   
     For every incoming request queried names are forwarded to these name servers to discover whether this service is authoritative or not. On successful response from listed name servers this service is responding to the original request with the list of all active swarm nodes. Otherwise it is responding without any data. 
     
     The response data provided by name servers isn't inspected in detail for it is assumed to refer to this service anyway.
     
     By default, this list of name servers is empty resulting in this service always responding to incoming requests.
     
     Results of looking up name servers are cached locally with TTL provided by name servers. If TTL is missing a default of 300 seconds is assumed.
      
   * `TRUSTED_ACCEPT_NODATA` is a boolean switch that **may** be used to prevent the service's default behaviour of assuming that answers without data - empty answers - are referrals to some name server and thus basically confirming this service being authoritative. 
   
     It is set by default and may be unset using some non-empty string which isn't evaluating as a truthy information.

   * `TRUSTED_REJECT_TIMEOUT` is another boolean switch that **may** be adjusted to consider timeouts on looking up trusted name servers being intentional for those name servers don't feel authoritative for requested name thus discouraging clients from ever asking again. 
   
     This option is enabled by default. So this service is assuming not to be authoritative for some incoming request if querying listed name servers times out. Otherwise, it is considering the timeout to be an error situation causing slightly different handling with regards to internal query caching.

4. Invoke the script to start the service eventually.

## Option: Using docker-compose

There is a option prepared to run the service with docker-compose instead of running custom script.

1. Create file **.env** on either server meant to answer DNS queries for your swarm. Put content found in [.env.dist](.env.dist) there and adjust it according to your setup.

2. Put file **docker-compose.yml** into the same folder with content found in [docker-compose.yml](docker-compose.yml).

3. Invoke `docker-compose up -d` for starting the service.

## Integration

Running this service isn't sufficient to get some working DNS infrastructure for your swarm-based services. In addition you need to have NS records in public DNS referring to this service on queries for domains to be managed in your swarm. 

This strongly depends on your particular domain provider and thus isn't covered in this short introduction.
