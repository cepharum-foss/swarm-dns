#!/bin/sh

STATUS_PROVIDER=http://your.swarm.com:3000/
LOCAL_NS=ns1.your.swarm.com,ns2.your.swarm.com
TRUSTED_NS=ns1.all.your.domains.com,ns2.all.your.domains.com
YOUR_IP=
UPDATE_INTERVAL=10
TRUSTED_ACCEPT_NODATA=y
TRUSTED_REJECT_TIMEOUT=y

###### end of configuration ##################################################

set -eu

[ -z "$(docker ps -qf name=swarm-dns)" ] || docker kill swarm-dns
[ -z "$(docker ps -qaf name=swarm-dns)" ] || docker rm swarm-dns

[ -n "$YOUR_IP" ] || YOUR_IP="$(LC_ALL=en ip addr show eth0 | awk '$1=="inet"{print$2}' | cut -d / -f 1)"

docker pull -q cepharum/swarm-dns

docker run -d \
	-e STATUS_PROVIDER="$STATUS_PROVIDER" \
	-e LOCAL_NS="$LOCAL_NS" \
	-e TRUSTED_NS="$TRUSTED_NS" \
	-e UPDATE_INTERVAL="$UPDATE_INTERVAL" \
	-e TRUSTED_ACCEPT_NODATA="$TRUSTED_ACCEPT_NODATA" \
	-e TRUSTED_REJECT_TIMEOUT="$TRUSTED_REJECT_TIMEOUT" \
	-p ${YOUR_IP}:53:53/udp \
	--name swarm-dns \
	--restart always \
	cepharum/swarm-dns
