FROM node:lts-alpine

LABEL maintainer="thomas.urban@cepharum.de"

COPY . /app/
RUN cd /app && npm ci

EXPOSE 53/udp

CMD ["node", "/app/server.js"]
